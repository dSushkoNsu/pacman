package ru.nsu.pacman.logic;

import org.junit.Before;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class MapCoordinateTest {

    private static final int INPUT_X = 100;
    private static final int INPUT_Y  = 100;

    private MapCoordinate mapCoordinate;

    @Before
    public void setUp() {
        mapCoordinate = new MapCoordinate(INPUT_X, INPUT_Y);
    }

    @Test
    public void testDecX() {
        mapCoordinate.decX();

        assertThat(mapCoordinate.getX()).isEqualTo(INPUT_X - 1);
    }

    @Test
    public void testIncX() {
        mapCoordinate.incX();

        assertThat(mapCoordinate.getX()).isEqualTo(INPUT_X + 1);
    }

    @Test
    public void testIncY() {
        mapCoordinate.incY();

        assertThat(mapCoordinate.getY()).isEqualTo(INPUT_Y + 1);
    }

}
