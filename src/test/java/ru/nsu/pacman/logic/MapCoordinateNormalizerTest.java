package ru.nsu.pacman.logic;

import org.junit.Before;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class MapCoordinateNormalizerTest {

    private static final int x_max = 100;
    private static final int y_max = 100;

    private MapCoordinateNormalizer mapCoordinateNormalizer;

    @Before
    public void setUp() {
        mapCoordinateNormalizer = new MapCoordinateNormalizer(x_max, y_max);
    }

    @Test
    public void inputPointInFieldThenSetSamePoint() {
        final int x_coordinate = 10;
        final int y_coordinate = 10;
        final MapCoordinate mapCoordinate = new MapCoordinate(x_coordinate, y_coordinate);
        final MapCoordinate expectMapCoordinate = new MapCoordinate(x_coordinate, y_coordinate);

        mapCoordinateNormalizer.normalize(mapCoordinate);

        assertThat(mapCoordinate).isEqualTo(expectMapCoordinate);
    }

    @Test
    public void inputPointCrossedField() {
        int x_coordinate = 101;
        int y_coordinate = 101;
        final MapCoordinate mapCoordinate = new MapCoordinate(x_coordinate, y_coordinate);

        x_coordinate = 1;
        y_coordinate = 1;
        final MapCoordinate expectMapCoordinate = new MapCoordinate(x_coordinate, y_coordinate);

        mapCoordinateNormalizer.normalize(mapCoordinate);

        assertThat(mapCoordinate).isEqualTo(expectMapCoordinate);
    }
}
