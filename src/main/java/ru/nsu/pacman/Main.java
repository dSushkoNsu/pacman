package ru.nsu.pacman;

import ru.nsu.pacman.ui.FieldPanel;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                PacmanGame driver = new PacmanGame();
            }
        });
    }
}


class PacmanGame extends JFrame {
    private static int width = 600;
    private static int height = 625;

    PacmanGame() {
        setTitle("Pacman");
        setResizable(false);
        setLayout(null);
        setSize(width, height);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        FieldPanel panel;
        try {
            panel = new FieldPanel();
        }
        catch(IOException ex) {
            System.out.println("Could not load files.");
            return;
        }

        getContentPane().add(panel);
        setVisible(true);
        panel.startGame();

    }
}