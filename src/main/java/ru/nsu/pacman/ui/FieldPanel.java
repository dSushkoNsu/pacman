package ru.nsu.pacman.ui;

import ru.nsu.pacman.logic.*;
import ru.nsu.pacman.logic.stillitems.EmptyField;
import ru.nsu.pacman.logic.units.GhostUnit;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class FieldPanel extends JPanel {
    public FieldPanel() throws IOException {
        setOpaque(true);
        setSize(width, height);
        setLayout(null);
        setFocusable(true);
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setBackground(Color.BLACK);
        addKeyListener(new DirectionHandler());
        fieldLogicProperties = new FieldReader(RESOURCE_PATH).readProperties();
        fieldLogicState = new PlayFieldState(fieldLogicProperties);
        fieldLogicRunner = new GameRunner(fieldLogicState);

        cellWidth = width / fieldLogicProperties.getWidth();
        cellHeight = height / fieldLogicProperties.getHeight();
        refreshLogicLocations();
        currentPacmanLocation = pacmanLocation;

        for (int i = 0; i < GHOST_COUNT; i++) {
            currentGhostLocation[i] = ghostLocation[i];
        }

        getImages();
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        setMap(g);
        setUnits(g);
    }

    private void setMap(Graphics g) {
        StillItem[][] stillItemsFromGame = fieldLogicState.getFieldState();
        EmptyField currentEmptyField;
        g.setColor(Color.BLUE);
        for (int i = 0; i < fieldLogicProperties.getWidth(); i++) {
            for (int j = 0; j < fieldLogicProperties.getHeight(); j++) {
                if (! (stillItemsFromGame[i][j] instanceof EmptyField)) {
                    g.fillRect(i*cellWidth, j*cellHeight, cellWidth, cellHeight);
                }
                else {
                    currentEmptyField = (EmptyField) stillItemsFromGame[i][j];
                    if (currentEmptyField.hasCoin()) {
                        g.setColor(Color.CYAN);
                        g.fillOval(i * cellWidth + (cellWidth / 2), j * cellHeight + (cellHeight / 2), cellWidth / 10, cellHeight / 10);
                        g.setColor(Color.BLUE);
                    }
                }
            }
        }
    }

    private void setUnits(Graphics g) {
        g.drawImage(workingPacmanImage,currentPacmanLocation.x,currentPacmanLocation.y,null);
        for (int i = 0; i < GHOST_COUNT; i++) {
            g.drawImage(workingGhostsImage[i],currentGhostLocation[i].x,currentGhostLocation[i].y,null);

        }
    }

    public void startGame() {
        synchronized (this) {
            mainTimer.scheduleAtFixedRate(new doEachItartion(), 0, MSEC_PER_ITARATION);
            littleTimer.scheduleAtFixedRate(new doMove(), 0, ((long) MSEC_PER_ITARATION) / cellWidth);
        }
    }

    private void getImages() throws IOException {
        int scaledImageSize = 0;
        if (cellHeight > cellWidth) {
            scaledImageSize = cellWidth;
        }
        else {
            scaledImageSize = cellHeight;
        }

        BufferedImage originalPacmanImage;
        BufferedImage[] originalGhostImage = new BufferedImage[GHOST_COUNT];
        String resultFileName = "pacman";
        switch(fieldLogicState.getPacmanUnitState().getDirection()) {
            case RIGHT :
                resultFileName += "right";
                break;
            case LEFT :
                resultFileName += "left";
                break;
            case UP :
                resultFileName += "up";
                break;
            case DOWN :
                resultFileName += "down";
                break;
            default:
                resultFileName += "right";
                break;
        }

        switch (pacmanMouth) {
            case OPEN:
                resultFileName += ".png";
                break;
            case CLOSED:
                resultFileName = "pacmanclosed.png";
                break;
        }

        originalPacmanImage = PacmanImageFactory.getImage(IMAGE_DIRECTORY + resultFileName);

        workingPacmanImage = originalPacmanImage.getScaledInstance(scaledImageSize,scaledImageSize,Image.SCALE_DEFAULT);


        for (int i = 0; i < GHOST_COUNT; i++) {
            switch(fieldLogicState.getGhostsState(i).getDirection()) {
                case RIGHT :
                    resultFileName = "slyde" + (i + 1) + "right.png";
                    break;
                case LEFT :
                    resultFileName =  "slyde" + (i + 1)+ "left.png";
                    break;
                case UP :
                    resultFileName =  "slyde" + (i + 1) + "up.png";
                    break;
                case DOWN :
                    resultFileName = "slyde" + (i + 1) + "down.png";
                    break;
                default:
                    resultFileName = "slyde" + (i + 1) + "right.png";
                    break;
            }
            originalGhostImage[i] = PacmanImageFactory.getImage(IMAGE_DIRECTORY+ resultFileName);
            workingGhostsImage[i] = originalGhostImage[i].getScaledInstance(scaledImageSize,scaledImageSize,Image.SCALE_DEFAULT);
        }
    }

    private void refreshLogicLocations() {
        countPacmanLocation();
        for (int i = 0; i < GHOST_COUNT; i++) {
            countGhostLocation(i);
        }
    }

    private class doEachItartion extends TimerTask {
        public void run() {
            if (pacmankilledFlag) {
                highScoreWriter.writeCoins(fieldLogicState.getTakenCoins());
                littleTimer.cancel();
                cancel();
            }

            if (directionChanged) {
                fieldLogicState.getPacmanUnitState().changeDirection(newDirection);
                directionChanged = false;
            }

            try {
                fieldLogicRunner.nextIteration();
                switch(pacmanMouth) {
                    case OPEN:
                        pacmanMouth = PacmanMouthState.CLOSED;
                        break;
                    case CLOSED:
                        pacmanMouth = PacmanMouthState.OPEN;
                        break;
                }
                getImages();
            }
            catch(Exception ex) { System.out.println("Exception caught while iterating"); }
            if (fieldLogicState.getPacmanUnitState().getHP() < 0) {
                pacmankilledFlag = true;
            }

            refreshLogicLocations();
            repaint();
        }
    }

    private class doMove extends TimerTask {
        public void run() {
            if ((pacmanLocation.x == 0) || (pacmanLocation.x == (fieldLogicProperties.getWidth() - 1)*cellWidth )) {
                currentPacmanLocation.x = pacmanLocation.x;
            }
            else if (pacmanLocation.x > currentPacmanLocation.x) {
                currentPacmanLocation.x++;
            }
            else if (pacmanLocation.x < currentPacmanLocation.x) {
                currentPacmanLocation.x--;
            }

            if ((pacmanLocation.y == 0) || (pacmanLocation.y == (fieldLogicProperties.getHeight() - 1)*cellHeight )) {
                currentPacmanLocation.y = pacmanLocation.y;
            }
            else if (pacmanLocation.y > currentPacmanLocation.y) {
                currentPacmanLocation.y++;
            }
            else if (pacmanLocation.y < currentPacmanLocation.y) {
                currentPacmanLocation.y--;
            }

            for (int i = 0; i < GHOST_COUNT; i++) {
                if ((ghostLocation[i].x == 0) || (ghostLocation[i].x == (fieldLogicProperties.getWidth() - 1)*cellWidth )) {
                    currentGhostLocation[i].x = ghostLocation[i].x;
                }
                else if (ghostLocation[i].x > currentGhostLocation[i].x) {
                    currentGhostLocation[i].x++;
                }
                else if (ghostLocation[i].x < currentGhostLocation[i].x) {
                    currentGhostLocation[i].x--;
                }

                if ((ghostLocation[i].y == 0) || (ghostLocation[i].y == (fieldLogicProperties.getHeight() - 1)*cellHeight )) {
                    currentGhostLocation[i].y = ghostLocation[i].y;
                }
                else if (ghostLocation[i].y > currentGhostLocation[i].y) {
                    currentGhostLocation[i].y++;
                }
                else if (ghostLocation[i].y < currentGhostLocation[i].y) {
                    currentGhostLocation[i].y--;
                }
            }
            repaint();
        }
    }

    private class DirectionHandler extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            switch(e.getKeyCode()) {
                case KeyEvent.VK_DOWN :
                    directionChanged = true;
                    newDirection = MovableItem.Direction.DOWN;
                    break;
                case KeyEvent.VK_UP :
                    directionChanged = true;
                    newDirection = MovableItem.Direction.UP;
                    break;
                case KeyEvent.VK_RIGHT :
                    directionChanged = true;
                    newDirection = MovableItem.Direction.RIGHT;
                    break;
                case KeyEvent.VK_LEFT :
                    directionChanged = true;
                    newDirection = MovableItem.Direction.LEFT;
                    break;
            }
        }
    }

    private void countPacmanLocation() {
        pacmanLocation = new Point(fieldLogicState.getPacmanUnitState().getCoordinate().getX() * cellWidth,
                fieldLogicState.getPacmanUnitState().getCoordinate().getY() * cellHeight);
    }

    private void countGhostLocation(int i) {
        ghostLocation[i] = new Point(fieldLogicState.getGhostsState(i).getCoordinate().getX() * cellWidth,
                fieldLogicState.getGhostsState(i).getCoordinate().getY() * cellHeight);
    }

    private transient FieldProperties fieldLogicProperties;
    private transient PlayFieldState fieldLogicState;
    private transient GameRunner fieldLogicRunner;
    private CoinFileWriter highScoreWriter = new CoinFileWriter(COINS_PATH);
    private final int width = 600;
    private final int height = 600;

    private int cellWidth;
    private int cellHeight;
    private transient Timer mainTimer = new Timer();
    private transient Timer littleTimer = new Timer();
    private transient Timer deathAnimationTimer = new Timer();

    private Point pacmanLocation = new Point();
    private Point[] ghostLocation =  new Point[GHOST_COUNT];

    private Point currentPacmanLocation = new Point();
    private Point[] currentGhostLocation = new Point[GHOST_COUNT];

    private transient Image workingPacmanImage;
    private transient Image[] workingGhostsImage = new Image[GHOST_COUNT];
    private enum PacmanMouthState {
        OPEN, CLOSED
    }
    private PacmanMouthState pacmanMouth = PacmanMouthState.OPEN;

    private transient ImageFactory PacmanImageFactory = new ImageFactory();
    private boolean pacmankilledFlag = false;

    private boolean directionChanged = false;
    private MovableItem.Direction newDirection;

    private static final String IMAGE_DIRECTORY = "resource/";
    private static final String RESOURCE_PATH = "resource/input_field.txt";
    private static final String COINS_PATH = "resource/scores.txt";

    final private static int MSEC_PER_ITARATION = 200;
    final private static int GHOST_COUNT = 3;

}