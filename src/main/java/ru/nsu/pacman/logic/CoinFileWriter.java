package ru.nsu.pacman.logic;


import java.io.*;
import java.util.Scanner;

public class CoinFileWriter {
    public CoinFileWriter(String fileName) {
        try {
            Scanner fileScanner = new Scanner(new FileInputStream(fileName));
            currentHighScore = fileScanner.nextInt();
            fileScanner.close();
            fileWriter = new BufferedWriter(new FileWriter(new File(fileName)));
        }
        catch (IOException ex) {
            System.err.println("Could not open file.");
        }
    }

    public void writeCoins(int inputCoins) {
        try {

            if (currentHighScore > inputCoins) {
                fileWriter.write(Integer.toString(currentHighScore));
            }
            else {
                fileWriter.write(Integer.toString(inputCoins));
            }
            fileWriter.flush();
        }
        catch(IOException ex) {
            System.err.println("Could not write high score.");
        }
    }


    private int currentHighScore = 0;
    private BufferedWriter fileWriter;
}
