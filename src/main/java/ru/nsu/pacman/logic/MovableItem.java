package ru.nsu.pacman.logic;

public abstract class MovableItem extends StillItem {
    public MovableItem(MapCoordinate input_coordinate, int x_max, int y_max) {
        super(input_coordinate, x_max, y_max);
    }

    public MapCoordinate moveForward() {
        switch (moves) {
            case DOWN:
                coordinate.incY();
                break;
            case LEFT:
                coordinate.decX();
                break;
            case RIGHT:
                coordinate.incX();
                break;
            case UP:
                coordinate.decY();
                break;
        }

        normalizer.normalize(coordinate);
        return coordinate;
    }

    public MapCoordinate moveBackwards() {
        switch(moves) {
            case DOWN:
                coordinate.decY();
                break;
            case LEFT:
                coordinate.incX();
                break;
            case RIGHT:
                coordinate.decX();
                break;
            case UP:
                coordinate.incY();
                break;
        }

        normalizer.normalize(coordinate);
        return coordinate;
    }

    public void changeDirection( Direction new_direction ) {
        moves = new_direction;
    }

    public Direction getDirection() {
        return moves;
    }

    public enum Direction {
        RIGHT, LEFT, UP, DOWN
    }

    private Direction moves = Direction.RIGHT;
}
