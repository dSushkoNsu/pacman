package ru.nsu.pacman.logic;

public abstract class StillItem extends MapItem {
    public StillItem(MapCoordinate coordinate, int x_max, int y_max) {
        super(coordinate, x_max, y_max);
    }
}
