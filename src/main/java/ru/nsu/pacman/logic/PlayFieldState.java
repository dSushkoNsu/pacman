package ru.nsu.pacman.logic;

import ru.nsu.pacman.logic.stillitems.EmptyField;
import ru.nsu.pacman.logic.stillitems.Wall;
import ru.nsu.pacman.logic.units.GhostUnit;
import ru.nsu.pacman.logic.units.PacmanUnit;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class PlayFieldState {

    private StillItem[][] field;
    private GhostUnit[] ghosts;
    private PacmanUnit pacman;
    private FieldProperties fieldProps;

    private int coinCounter = 0;
    private int initialCoinsCounter = 0;

    public PlayFieldState(FieldProperties inputFieldProps) {
        fieldProps = inputFieldProps;
        setField();
        locateGhosts();
        locatePacman();
    }

    private void locatePacman() {
        pacman = new PacmanUnit(
                fieldProps.getPacmanLocation(),
                fieldProps.getWidth(),
                fieldProps.getHeight()
        );
    }

    private void locateGhosts() {
        ghosts = new GhostUnit[FieldProperties.AMOUNT_OF_GHOSTS];
        for (int i = 0; i < FieldProperties.AMOUNT_OF_GHOSTS; i++) {
            ghosts[i] = new GhostUnit(
                    fieldProps.getGhostLocation(i),
                    fieldProps.getWidth(),
                    fieldProps.getHeight()
            );
        }
    }

    public int getTakenCoins() { return initialCoinsCounter - coinCounter; }

    private void setField() {
        int width = fieldProps.getWidth();
        int height = fieldProps.getHeight();
        field = new StillItem[width][height];
        boolean[][] fieldItems = fieldProps.getFieldItems();
        EmptyField currentEmptyField;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (fieldItems[i][j]) {
                    field[i][j] = new Wall(
                            new MapCoordinate(i,j),
                            width,
                            height
                    );
                }
                else {
                    field[i][j] = new EmptyField(
                            new MapCoordinate(i,j),
                            width,
                            height
                    );
                    currentEmptyField = (EmptyField)field[i][j];
                    currentEmptyField.placeCoin();
                    coinCounter++;
                }
            }
        }
        initialCoinsCounter = coinCounter;
    }

    int getCoins() {
        return coinCounter;
    }

    void decCoin() {
        coinCounter--;
    }

    public PacmanUnit getPacmanUnitState() {
        return pacman;
    }

    GhostUnit[] getGhostsUnits() { return ghosts; }

    public GhostUnit getGhostsState(int index) {
        return ghosts[index];
    }

    public StillItem[][] getFieldState() {
        return field;
    }
}
