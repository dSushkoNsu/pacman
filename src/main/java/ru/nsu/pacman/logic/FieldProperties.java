package ru.nsu.pacman.logic;

public class FieldProperties {
    static int AMOUNT_OF_GHOSTS = 4;
    private static MapCoordinate INITIAL_COORDINATE = new MapCoordinate(0,0);

    private int height = 0;
    private int width = 0;

    private MapCoordinate pacmanCoordinate = INITIAL_COORDINATE;
    private MapCoordinate[] ghostsCoordinates = new MapCoordinate[AMOUNT_OF_GHOSTS];
    private boolean[][] fieldItems = new boolean[width][height];

    void setPacmanLocation(int input_x, int input_y) {
        pacmanCoordinate = new MapCoordinate(input_x, input_y);
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

   void setHeight(int input_height) {
        height = input_height;
    }

    void setWidth(int input_width) {
        width = input_width;
    }

    void setGhostsLocation(int index, int input_x, int input_y) {
        if (index >= 0 && index < AMOUNT_OF_GHOSTS) {
            ghostsCoordinates[index] = new MapCoordinate(input_x, input_y);
        }
    }

    void setFieldItems(boolean[][] input_field_items) {
        fieldItems = input_field_items;
    }

    MapCoordinate getPacmanLocation() {
        return pacmanCoordinate;
    }

    MapCoordinate getGhostLocation(int index) {
        return ghostsCoordinates[index];
    }

    boolean[][] getFieldItems() {
        return fieldItems;
    }

}
