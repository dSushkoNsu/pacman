package ru.nsu.pacman.logic;

public abstract class MapItem {

    MapItem(MapCoordinate input_coordinate, int x_max, int y_max) {
        coordinate = input_coordinate;
        normalizer = new MapCoordinateNormalizer(x_max, y_max);
        normalizer.normalize(coordinate);
    }

    public MapCoordinate getCoordinate() {
        return coordinate;
    }

    MapCoordinateNormalizer getNormalizer() {
        return normalizer;
    }

    protected MapCoordinate coordinate;

    protected MapCoordinateNormalizer normalizer;
}
