package ru.nsu.pacman.logic;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FieldReader {
    private static final char PACMAN_SYMBOL = '(';
    private static final char WALL_SYMBOL = '*';
    private static final char EMPTY_SPACE_SYMBOL = '-';
    private static final char GHOSTS_SYMBOL = 'x';

    private FieldProperties fieldProps = new FieldProperties();
    private Scanner textsScanner = null;

    private void readWidth() {
        fieldProps.setWidth(textsScanner.nextInt());
    }

    private void readHeight() {
        fieldProps.setHeight(textsScanner.nextInt());
    }

    private void readField() {
        int width = fieldProps.getWidth();
        int height = fieldProps.getHeight();
        boolean[][] fieldItems = new boolean[width][height];
        int heightCounter = 0;
        String currentLine = textsScanner.nextLine();
        char currentToken;
        int ghostsFound = 0;
        while  ((heightCounter < height) && (textsScanner.hasNextLine())) {
            currentLine = textsScanner.nextLine();
            for (int i = 0; i < width; i++) {
                currentToken =  currentLine.charAt(i);
                switch (currentToken) {
                    case PACMAN_SYMBOL :
                        fieldProps.setPacmanLocation(i, heightCounter);
                        break;
                    case GHOSTS_SYMBOL :
                        fieldProps.setGhostsLocation(ghostsFound, i, heightCounter);
                        ghostsFound++;
                        break;
                    case WALL_SYMBOL :
                        fieldItems[i][heightCounter] = true;
                        break;
                    case EMPTY_SPACE_SYMBOL :
                        fieldItems[i][heightCounter] = false;
                        break;
                }
            }
            heightCounter++;
        }

        fieldProps.setFieldItems(fieldItems);
    }

    public FieldReader(String filePath) throws FileNotFoundException {
        textsScanner = new Scanner(new FileInputStream(filePath));
    }

    public FieldProperties readProperties() {
        readWidth();
        readHeight();
        readField();
        return fieldProps;
    }
}
