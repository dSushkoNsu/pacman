package ru.nsu.pacman.logic;

public class MapCoordinateNormalizer {
    MapCoordinateNormalizer(int x_max, int y_max) {
        x_maximum = x_max;
        y_maximum = y_max;
    }

    public MapCoordinate normalize(MapCoordinate input_coordinate) {
        int outputX = input_coordinate.getX();
        int outputY = input_coordinate.getY();

        if (outputX < 0) {
            outputX = x_maximum - 1 ;
        }
        if (outputY < 0) {
            outputY = y_maximum - 1;
        }

        outputX %= x_maximum;
        outputY %= y_maximum;

        input_coordinate.setX(outputX);
        input_coordinate.setY(outputY);

        return input_coordinate;
    }

    private int x_maximum;
    private int y_maximum;
}
