package ru.nsu.pacman.logic;

import ru.nsu.pacman.logic.stillitems.EmptyField;
import ru.nsu.pacman.logic.units.GhostUnit;
import ru.nsu.pacman.logic.units.PacmanUnit;

import static java.lang.Math.abs;

public class GameRunner {
    private PlayFieldState fieldState;

    private boolean isPossibleToMove(MovableItem unit) {
        MapCoordinate nextCoordinate = unit.getNormalizer().normalize(unit.moveForward());

        if (fieldState.getFieldState()[nextCoordinate.getX()][nextCoordinate.getY()] instanceof EmptyField) {
            unit.moveBackwards();
            return true;
        }

        unit.moveBackwards();
        return false;
    }

    public GameRunner(PlayFieldState input_field) {
        fieldState = input_field;
    }

    public int nextIteration() throws Exception {
        PacmanUnit pacman = fieldState.getPacmanUnitState();
        GhostUnit[] ghosts = fieldState.getGhostsUnits();
        MapCoordinate pacmanCoordinate = pacman.getCoordinate();
        EmptyField currentPacmanField = (EmptyField)fieldState.getFieldState()[pacmanCoordinate.getX()][pacmanCoordinate.getY()];
        if (currentPacmanField.hasCoin()) {
            currentPacmanField.takeCoin();
            fieldState.decCoin();
            if (fieldState.getCoins() <= 0) {
                fieldState.getPacmanUnitState().setHP(-1);
            }
        }
        if (fieldState.getCoins() <= 0) {
            fieldState.getPacmanUnitState().setHP(-1);
        }

        if ( isPossibleToMove(pacman) ) {
            pacman.moveForward();
        }

        for (int i = 0; i < 3; i++) {
            MapCoordinate ghostCoordinate = ghosts[i].getCoordinate();
            int dx = ghostCoordinate.getX() - pacmanCoordinate.getX();
            int dy = ghostCoordinate.getY() - pacmanCoordinate.getY();
            while (!isPossibleToMove(ghosts[i])) {
                if ((ghosts[i].getDirection() == MovableItem.Direction.LEFT) ||
                    (ghosts[i].getDirection() == MovableItem.Direction.RIGHT)) {
                    if (abs(dy) > abs(dx)) {
                        if (dy > 0) {
                            ghosts[i].changeDirection(MovableItem.Direction.UP);
                        }
                        else {
                            ghosts[i].changeDirection(MovableItem.Direction.DOWN);
                        }
                    }
                    else if (ghosts[i].getDirection() == MovableItem.Direction.RIGHT) {
                        ghosts[i].changeDirection(MovableItem.Direction.LEFT);
                    }
                    else {
                        ghosts[i].changeDirection(MovableItem.Direction.RIGHT);
                    }
                }
                else {
                    if (abs(dx) > abs(dy)) {
                        if (dx > 0) {
                            ghosts[i].changeDirection(MovableItem.Direction.RIGHT);
                        }
                        else {
                            ghosts[i].changeDirection(MovableItem.Direction.LEFT);
                        }
                    }
                    else if (ghosts[i].getDirection() == MovableItem.Direction.UP) {
                        ghosts[i].changeDirection(MovableItem.Direction.DOWN);
                    }
                    else {
                        ghosts[i].changeDirection(MovableItem.Direction.UP);
                    }
                }
            }
            ghosts[i].moveForward();
            if (ghostCoordinate.getX() == pacmanCoordinate.getX() &&
                    ghostCoordinate.getY() == pacmanCoordinate.getY()) {
                pacman.kill();
            } else if ((ghostCoordinate.getX() + dx == pacmanCoordinate.getX()) &&
                    ghostCoordinate.getY() + dy == pacmanCoordinate.getY()) {
                if (ghosts[i].getDirection() != pacman.getDirection()) {
                    pacman.kill();
                }
            }
        }
        return pacman.getHP();
    }

    public void changePacmanDirection(MovableItem.Direction new_direction) {
        fieldState.getPacmanUnitState().changeDirection(new_direction);
    }

    public PlayFieldState getPlayField() {
        return fieldState;
    }
}
