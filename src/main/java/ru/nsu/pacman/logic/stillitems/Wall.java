package ru.nsu.pacman.logic.stillitems;

import ru.nsu.pacman.logic.MapCoordinate;
import ru.nsu.pacman.logic.StillItem;

public class Wall extends StillItem {
    public Wall(final MapCoordinate coordinate, final int x_max, final int y_max) {
        super(coordinate, x_max, y_max);
    }
}
