package ru.nsu.pacman.logic.stillitems;

import ru.nsu.pacman.logic.MapCoordinate;
import ru.nsu.pacman.logic.StillItem;

public class EmptyField extends StillItem {
    public EmptyField(final MapCoordinate input_coordinate, final int xm, final int ym) {
        super(input_coordinate, xm, ym);
    }

    public void placeCoin() {
        coin = true;
    }

    public void takeCoin() {
        coin = false;
    }

    public boolean hasCoin() {
        return coin;
    }

    private boolean coin = false;

}
