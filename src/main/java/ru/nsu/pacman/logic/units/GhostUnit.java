package ru.nsu.pacman.logic.units;

import ru.nsu.pacman.logic.MapCoordinate;
import ru.nsu.pacman.logic.MovableItem;


public class GhostUnit extends MovableItem {
    public enum State {
        DEAD, ALIVE
    }
    
    public GhostUnit(MapCoordinate input_coordinate, int xm, int ym) {
        super(input_coordinate,xm,ym);
    }

    public void changeState() {
        state = (state == State.ALIVE) ? State.DEAD : State.ALIVE;
    }

    public State getState() {
        return state;
    }

    public void replaceOnBirthSpot(MapCoordinate birth_coordinate) {
        coordinate = birth_coordinate;
        normalizer.normalize(coordinate);
    }

    private State state;
}
