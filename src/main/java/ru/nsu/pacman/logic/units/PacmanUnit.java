package ru.nsu.pacman.logic.units;

import ru.nsu.pacman.logic.MapCoordinate;
import ru.nsu.pacman.logic.MovableItem;

public class PacmanUnit extends MovableItem {
    public PacmanUnit(MapCoordinate input_coordinate, int xm, int ym) {
        super(input_coordinate, xm, ym);
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int new_hp) {
        HP = new_hp;
    }

    public void kill() {
        HP--;
    }

    private int HP = 4;

}
