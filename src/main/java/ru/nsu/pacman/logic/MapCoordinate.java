package ru.nsu.pacman.logic;

import com.google.common.base.Objects;

public class MapCoordinate {
    public MapCoordinate(int input_x, int input_y) {
        x = input_x;
        y = input_y;
    }

    public void setX(int input_x) {
        x = input_x;
    }

    public void setY(int input_y) {
        y = input_y;
    }

    public void incX() {
        x++;
    }

    public void incY() {
        y++;
    }

    public void decX() {
        x--;
    }

    public void decY() {
        y--;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    private int x;
    private int y;

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == this.getClass()) {
            final MapCoordinate mapCoordinate = (MapCoordinate) obj;

            return this.x == mapCoordinate.x && this.y == mapCoordinate.y;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(x, y);
    }
}
